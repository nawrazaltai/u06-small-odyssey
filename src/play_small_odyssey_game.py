from sqlite3 import Error
import os.path
import shutil
import help_fuctions


def play_small_odyssey_game():
    """
    play_small_odyssey_game is a function to play a small odyssey game.

    It will check if a save already exist and use it to continue the game.
    If no save exist it will create one from a created game and will
    end this script if no game exist and print out text to inform user that no game exist.
    Then it will start the play game loop with the save file where 
    the user plays the game by reading the text printed out and
    making choice based on numberd list with the option to 
    end the game by selecting 0.

    return: True if all went well.
    """

    game_file_name = r"my_game.sog" # name of game file
    save_file_name = r"my_game.sos" # name of save file

    # Check if save exist, if not create save file from game file.
    do_save_already_exist = os.path.exists(save_file_name) # Check if save file already exist.
    if not do_save_already_exist:
        # check if a game exist, and if i do not exist print out to inform user and return True to end game.
        do_game_already_exist = os.path.exists(game_file_name) # Check if game file already exist.
        if not do_game_already_exist:
            print("No game exist. Create one first and then you can play it.")
            return True
        shutil.copyfile(game_file_name, save_file_name)

    db_conn = help_fuctions.create_connection(save_file_name) # create a database connection to save file

    play_game_main_loop(db_conn)

    return True


def play_game_main_loop(db_conn):
    """
    Main loop of the game.
    The player will se text for current location andbe able to makea choice based on current location.
    A choice may change the player current location.
    Loop until player wish to exit by choicing 0.
    A location that is the end of the game(can be a end of the game or game over) will 
    have no choice leading to player only beeing able to exit the game.

    TODO: add so player se more text after location text then state is correct for it.
    PREREQUISITE: Need table LocationExtraText and State to do.
    TODO: add so only correct choice is listed based on state if it has any.
    PREREQUISITE: Need table State to do.
    TODO: add so choice that change state do change state.
    PREREQUISITE: Need table State and ActionChangeState to do.
    
    param: 
        db_conn: the Connection object
    return: True if all went well.
    """

    while True:
        player_location = help_fuctions.get_player_location(db_conn)

        location_id = player_location[0]
        list_of_chocie = help_fuctions.get_all_choice_for_one_location(db_conn, location_id)
        
        # Prepare list_player_choice fild with all the choice the player can make for current location. 
        list_player_choice = []
        for choice in list_of_chocie:
            list_player_choice.append(choice[1])
        
        location_text = player_location[2]
        exit_choice_text = "Exit the game."
        user_input = help_fuctions.user_number_choice(location_text, list_player_choice, exit_choice_text)

        # If user selected 0, end the game by return True
        if user_input == 0:
            return True

        # print text from choice for the choice the player selected.
        selected_chocie_text = list_of_chocie[user_input - 1][2]
        print(f"\n{selected_chocie_text}\n") 

        # change player location if this choice have a ActionChangeLocation.
        selected_choie_id = list_of_chocie[user_input - 1][0]
        help_fuctions.change_player_location_based_on_choice(db_conn, selected_choie_id)

# if this file is run, call create_small_odyssey_game fuction.
if __name__ == '__main__':
    play_small_odyssey_game()

