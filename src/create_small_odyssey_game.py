from sqlite3 import Error
import os.path
import help_fuctions


# TABLE_NAMES_TO_WORK_ON is a list of all tabels that user will be able to work on will making the game.
TABLE_NAMES_TO_WORK_ON = ("Location", "Choice", "ActionChangeLocation")





def create_small_odyssey_game():
    """
    Create_small_odyssey_game is a function to creating a small odyssey game.

    This is done by use a sqlite3 database what will be created(based on sql file core_schema.sql) or 
    used if already existing.
    Then it will call fuction that handle the main loop that contain all the 
    interation where the user can view, add, change and delete content for the game and view help that explain what the user 
    need to know about creating a small odyssey game.

    return: True if all went well.
    """

    game_file_name = r"my_game.sog" # name of game file

    did_game_already_exist = os.path.exists(game_file_name) # Check if game file already exist.
    db_conn = help_fuctions.create_connection(game_file_name) # create a database connection to game file and create game file if i did not already exist

    if not did_game_already_exist: # if the game did not exist it will need to be filed with core basic content from core_schema.sql.
        with open('core_schema.sql') as f:
            sql_core = f.read()

            try:
                cur = db_conn.cursor()
                cur.executescript(sql_core)
            except Error as e:
                print(e)

    create_game_main_loop(db_conn)

    return True

def create_game_main_loop(db_conn):
    """
    create_game_main_loop is the main loop that handle interation with use to create a small odyssey game.
    It will allow the player to add, change and delete content for the game and exit when use is done working on the game.

    param: 
        db_conn: database connection to a sqlite database what can used for a small odyssey game.
    return: True if all went well.
    """

    while True:

        choice_text = "What part of the game do you wish to work on"
        exit_choice_text = "Exit, stop working on the game"
        user_input = help_fuctions.user_number_choice(choice_text, TABLE_NAMES_TO_WORK_ON, exit_choice_text)
        print("user_input is:", user_input) # TEST CODE

        if user_input == 0:
            return True

        # User working on selected table loop by adding updating and deleting to selected table until user is done with it.
        rows = help_fuctions.work_on_table_loop(db_conn, TABLE_NAMES_TO_WORK_ON[user_input-1]) # user_input-1 for index start at 0. This will make the number choice match the right table name


# if this file is run, call create_small_odyssey_game fuction.
if __name__ == '__main__':
    create_small_odyssey_game()
